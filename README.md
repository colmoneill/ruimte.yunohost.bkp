# Ruimte YUNOHOST confing & backup files

* fstab for external storage at /etc/fstab
* noip configs
* cronjobs
	* mostly moving dowloads from transmission folder to ext storage


## noip notes:

* install the DUC (Dynamic Update Client) in the admin user's home following this https://www.noip.com/support/knowledgebase/install-ip-duc-onto-raspberry-pi/

* check if it's working by running `sudo /usr/local/bin/noip2`

* `sudo noip2 -S` gives status of the running process

* starting the process on boot is done as so:

```
sudo touch /etc/init.d/noip2
sudo chmod 755 /etc/init.d/noip2
sudo update-rc.d noip2 defaults
```

and restore the /etc.init.d/noip2 file from this repo

## transmission to airsonic cron:

* place the mv-dls-to-airsonic script in the admin user home

* edit the contab to include the .crontab file to run every 15 mins
